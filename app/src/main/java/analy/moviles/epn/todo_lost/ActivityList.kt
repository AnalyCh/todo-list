package analy.moviles.epn.todo_lost

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.InputDevice
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase

import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class ActivityList : AppCompatActivity(),
        ListSelectionRecyclerViewAdapter.listSelectionRecyclerViewClickListener
{


    lateinit var listaRecycleView: RecyclerView
    val database = FirebaseDatabase.getInstance();
    val ref = database.getReference("todo-list")

    companion object {
        val INTENT_LIST_ID = "listid"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)



        //RecyclerView
        //#recycler view --> view holder --> (adapter) (Revisa si hay que crearlo o solo cargarlo)
        // As the user scrolls the list, the RecyclerView creates new view holders as necessary. It also saves the view holders which have scrolled off-screen, so they can be reused.

        listaRecycleView = findViewById(R.id.list_recyclerView)     //conectar variable con la vista
        listaRecycleView.layoutManager = LinearLayoutManager(this)  // Para que se vea en horizontal Lineal == vertical --> conecta el recycleView a uno lineal(1 dimensión)    GRID --> 2 dimensiones
        listaRecycleView.adapter = ListSelectionRecyclerViewAdapter(ref  ) //, thises una instancia de ListSelectionRecyclerViewAdapteradapter a crear


        fab.setOnClickListener { view ->
            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
              //      .setAction("Action", null).show()
            showCreateListDialog()
        }
    }

    private fun showCreateListDialog(){
        //take the strings variables
        val dialogTitle = getString(R.string.name_of_list)
        val positiveButtonTitle = getString(R.string.create_list)

        val builder = AlertDialog.Builder(this)

        //create a view of type editText
        val listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(dialogTitle)

        builder.setView(listTitleEditText)

        builder.setPositiveButton(positiveButtonTitle){dialog, i ->
            val newList = listTitleEditText.text.toString()
            //la llave todoList, con el .child()-> que sea único= SI NO EXISTE SE CREA
            //SIEMPRE PARA TRABJAR ID´S desde nuestro codigo usar UUID
            //cada vez que creamos un nuevo item se genera un nuevo UUID
            ref.child(UUID.randomUUID().toString()).child("List-name").setValue(newList)
            dialog.dismiss()
        }

        builder.create().show()


    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showListDetail(listID: String){
        val listDetailIntent = Intent(this,ListSelectionRecyclerViewHolder::class.java ) //ListDetailActivity
        listDetailIntent.putExtra(INTENT_LIST_ID, listID)
        startActivity(listDetailIntent)
    }

    override fun listItemCLicked() {
        showListDetail(todoList.id)
    }

  //  override fun listItemClicked(todoList: TodoList){

}

//  |@#¢∞¬÷“”≠´‚][{~

//Patron de diseño delegado --> decirle a otra clase que haga algo y te avise.
//Otr
//head first design patterns
//*Builder
//*Delegado
//*Singleton